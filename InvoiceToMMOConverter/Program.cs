﻿using System;
//using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;

using log4net;
using log4net.Config;

using InvoiceToMMOConverter.Data;
using InvoiceToMMOConverter.Forms;
using InvoiceToMMOConverter.Logic;
using InvoiceToMMOConverter.Properties;

namespace InvoiceToMMOConverter
{
    static class Program
    {
        /// <summary>
        /// Main App Controler.
        /// </summary>
        static Controler MainControler;

        /// <summary>
        /// Main App Importer of XLS Files.
        /// </summary>
        //static ExcelXlsImporter MainImporter;
        static XlsImporter MainImporter;

        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                // System Calls
                //CultureInfo.DefaultThreadCurrentCulture = CultureInfo.GetCultureInfo("ru-UA");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                // Configuring the logger
                XmlConfigurator.Configure();

                // Controler assemly
                //MainImporter = new ExcelXlsImporter();
                MainImporter = new XlsImporter();
                MainControler = new Controler(MainImporter);

                // Settings configuration
                if (Settings.Default.update_required)
                {
                    // For the first app launch
                    if (
                        (Settings.Default.GetPreviousVersion("first_launch") == null) ||
                        ((bool)Settings.Default.GetPreviousVersion("first_launch"))
                        )
                    {
                        // Paths calculations
                        string appPath = AppDomain.CurrentDomain.BaseDirectory;

                        Directory.CreateDirectory(Path.Combine(appPath, "logs"));
                        Directory.CreateDirectory(Path.Combine(appPath, "import"));
                        Directory.CreateDirectory(Path.Combine(appPath, "export"));
                        log.Info("First app launch. Making neccessary steps...");
                        log.Info("Folder Infrastructure checked and created.");

                        Settings.Default.log_dir = Path.Combine(appPath, "logs");
                        Settings.Default.import_dir = Path.Combine(appPath, "import");
                        Settings.Default.export_dir = Path.Combine(appPath, "export");

                        Settings.Default.first_launch = false;
                        Settings.Default.Save();
                        log.Info("Settings saved successfully.");
                    }
                    else
                    {
                        log.Info("App updated to version " + Settings.Default.app_version + ". Making neccessary steps...");
                        Settings.Default.Upgrade();
                        Settings.Default.update_required = false;
                        Settings.Default.Save();
                    }
                }


                // Checking logs to delete week-old ones
                System.Collections.Generic.List<string> logs = new System.Collections.Generic.List<string>(
                    Directory.GetFiles(Settings.Default.log_dir)
                    );
                foreach (string logFile in logs)
                {
                    string[] logDateString = Path.GetFileNameWithoutExtension(logFile).Split('.');

                    DateTime logDate = new DateTime(
                        int.Parse(logDateString[0]),
                        int.Parse(logDateString[1]),
                        int.Parse(logDateString[2])
                        );

                    if ((DateTime.Now - logDate).Days >= 7)
                    {
                        File.Delete(logFile);
                    }
                }

                // If on auto mode
                if (Settings.Default.auto_mod)
                {
                    log.Info("App launched in auto mode. Initializing.");

                    //Load Clients Info
                    if (MetadataStorage.LoadClients())
                    {
                        // Grab everything from import directory
                        System.Collections.Generic.List<string> files = new System.Collections.Generic.List<string>(
                            Directory.GetFiles(Settings.Default.import_dir)
                            );

                        if (files.Count > 0)
                        {
                            if (Settings.Default.log_debug)
                            { log.Debug(files.Count + " files found inside import folder. Filtering..."); }

                            // Filter out non-Excel files
                            int filesCountOld = files.Count;
                            files = files.Where(
                                x => x.EndsWith(".xls") || x.EndsWith(".xlsx")
                                ).ToList();
                            if (Settings.Default.log_debug)
                            {
                                if (filesCountOld - files.Count > 0)
                                { log.Debug(files.Count + " files left after filtering."); }
                                else
                                { log.Debug("No files were filtered out. File list intact."); }
                            }

                            // Do the thing
                            try
                            {
                                bool totalResult = false;

                                SplashForm splash = new SplashForm();
                                splash.ShowInTaskbar = true;
                                splash.Show();

                                if (MainControler.ImportFiles(files.ToArray()))
                                {
                                    if (MainControler.ProccesFiles())
                                    {
                                        if (MainControler.ExportFiles())
                                        {
                                            totalResult = true;
                                        }
                                    }
                                }

                                splash.Close();

                                // Reflect on the results
                                if (totalResult)
                                {
                                    // Delete original files after them being converted if enabled in settings
                                    if (Settings.Default.delete_after)
                                    {
                                        foreach (string file in files)
                                        {
                                            File.Delete(file);
                                        }

                                        log.Info("Source files deleted after convertion.");
                                    }

                                    MessageBox.Show(
                                        DateTime.Now.ToShortTimeString() + ": Файл(ы) успешно конвертирован(ы)!",
                                        "Успех!",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show(
                                        DateTime.Now.ToShortTimeString() + ": Во время процесса возникла ошибка!",
                                        "Ошибка!",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                                }
                            }
                            catch (Exception exc)
                            {
                                if (exc.InnerException == null)
                                {
                                    log.Error("Error Occured: " + exc.Message, exc);

                                    MessageBox.Show(
                                        exc.Message +
                                        "\n=======================\n" +
                                        exc.StackTrace);
                                }
                                else
                                {
                                    if (exc.InnerException.InnerException == null)
                                    {
                                        log.Error("Error Occured: " + exc.InnerException.Message, exc.InnerException);

                                        MessageBox.Show(
                                            exc.InnerException.Message + "\n" +
                                            exc.Message +
                                            "\n=======================\n" +
                                            exc.StackTrace);
                                    }
                                    else
                                    {
                                        log.Error("Error Occured: " + exc.InnerException.InnerException.Message, exc.InnerException.InnerException);

                                        MessageBox.Show(
                                            exc.InnerException.InnerException.Message + "\n" +
                                            exc.InnerException.Message + "\n" +
                                            exc.Message +
                                            "\n=======================\n" +
                                            exc.StackTrace);
                                    }
                                }
                            }
                            finally
                            {
                                log.Info("Exiting the app...\n");
                                Application.Exit();
                            }
                        }
                        else
                        {
                            log.Info("No files in import folder. Proceeding with manual mode.");

                            // Proceed with manual mode
                            Application.Run(new MainForm(MainControler));
                        }
                    }
                    else
                    {
                        log.Error("Error while loading client info! Application will shut down.");
                        log.Info("Exiting the app...\n");
                        Application.Exit();
                    }
                }
                else
                {
                    log.Info("App launched in manual mode. Initializing...");

                    //Load Clients Info
                    if (MetadataStorage.LoadClients())
                    {
                        // Proceed with manual mode
                        Application.Run(new MainForm(MainControler));
                    }
                    else
                    {
                        log.Error("Error while loading client info! Application will shut down.");
                        log.Info("Exiting the app...\n");
                        Application.Exit();
                    }
                }
            }
            catch (Exception exc)
            {
                if (exc.InnerException == null)
                {
                    MessageBox.Show(
                        exc.Message + 
                        "\n=======================\n" + 
                        exc.StackTrace);
                }
                else
                {
                    if (exc.InnerException.InnerException == null)
                    {
                        MessageBox.Show(
                            exc.InnerException.Message + "\n" + 
                            exc.Message + 
                            "\n=======================\n" + 
                            exc.StackTrace);
                    }
                    else
                    {
                        MessageBox.Show(
                            exc.InnerException.InnerException.Message + "\n" + 
                            exc.InnerException.Message + "\n" + 
                            exc.Message + 
                            "\n=======================\n" + 
                            exc.StackTrace);
                    }
                }
            }
        }
    }
}
