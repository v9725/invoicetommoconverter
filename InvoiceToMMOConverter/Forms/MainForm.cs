﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using log4net;

using InvoiceToMMOConverter.Forms;

namespace InvoiceToMMOConverter
{
    /// <summary>
    /// Main form of the application.
    /// </summary>
    public partial class MainForm : Form
    {
        #region Fields

        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(MainForm));

        /// <summary>
        /// Контроллер приложения.
        /// </summary>
        private Controler Controler;

        #region Buttons ToolTips
        private string tooTipBrowseFilesButton = 
            "Выбор файлов накладных для конвертации.\n"+
            "Накладные должны быть в формате .xls";

        private string toolTipConvertFilesButton = 
            "Конвертирует вырбранные файлы накладных.\n"+
            "Процесс занимает какое-то время в зависимости от количества файлов.";

        private string toolTipSettingsButton = 
            "Открывает окно настроек напрограммы";

        private string toolTipAboutButton = 
            "Открывеет окно \"О программе\"";
        #endregion

        #endregion

        #region Constructor and Main events

        /// <summary>
        /// Form contructor.
        /// </summary>
        public MainForm(Controler controler)
        {
            InitializeComponent();

            // Assigning the controler
            Controler = controler;

            // Setting the tooltips
            ToolTip browseTT = new ToolTip();
            browseTT.SetToolTip(BrowseFilesButton, tooTipBrowseFilesButton);

            ToolTip convertTT = new ToolTip();
            convertTT.SetToolTip(ConvertFilesButton, toolTipConvertFilesButton);

            ToolTip settingsTT = new ToolTip();
            settingsTT.SetToolTip(SettingsButton, toolTipSettingsButton);

            ToolTip aboutTT = new ToolTip();
            aboutTT.SetToolTip(AboutButton, toolTipAboutButton);

            // Setting other visuals
            this.Icon = Properties.Resources.refresh_icon;
            ConvertFilesButton.Enabled = false;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            log.Info("Exiting the app...\n");
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            log.Info("Initialiaing GUI...");
        }

        #endregion

        #region ButtonsHandlers

        private void ButtonBrowseFiles_Click(object sender, EventArgs e)
        {
            if (Controler.BrowseXLSFiles())
            {
                FilesListDataGrid.DataSource = Controler.FilesToConvert.Select(x => new { Value = x }).ToList();
                FilesListDataGrid.CurrentCell = null;

                foreach (DataGridViewColumn column in FilesListDataGrid.Columns)
                {
                    column.Name = "Список файлов";
                    column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }

                int rowindex = 1; //Переменная для номера строки
                foreach (DataGridViewRow row in FilesListDataGrid.Rows)
                {
                    row.Resizable = DataGridViewTriState.False;
                    row.HeaderCell.Value = string.Format("{0}", rowindex++);
                }

                ConvertFilesButton.Enabled = true;
            }
        }

        private void ButtonConvertFiles_Click(object sender, EventArgs e)
        {
            // Setting the visuals
            FilesListDataGrid.Enabled = false;
            FilesListDataGrid.CurrentCell = null;
            AboutButton.Enabled = false;
            BrowseFilesButton.Enabled = false;
            ConvertFilesButton.Enabled = false;
            SettingsButton.Enabled = false;

            this.Cursor = Cursors.WaitCursor;

            // Centrering and showing "Proccessing..." splash form
            SplashForm splash = new SplashForm();
            splash.StartPosition = FormStartPosition.Manual;
            splash.Location = new System.Drawing.Point(
                this.Location.X + (this.Width - splash.Width) / 2, 
                this.Location.Y + (this.Height - splash.Height) / 2
                );
            splash.Show(this);


            bool result = false;

            // Asking Controler to do its job
            if (Controler.ImportFiles())
            {
                if (Controler.ProccesFiles())
                {
                    if (Controler.ExportFiles())
                    {
                        result = true;
                    }
                }
            }

            // Reflecting on the results
            if(result)
            {
                this.Cursor = Cursors.Default;

                MessageBox.Show(
                    DateTime.Now.ToShortTimeString() + " Файл(ы) успешно конвертирован(ы)!",
                    "Успех!",
                    MessageBoxButtons.OK);
            }
            else
            {
                this.Cursor = Cursors.Default;

                MessageBox.Show(
                    DateTime.Now.ToShortTimeString() + " Во время процесса возникла ошибка!\nПожалуйста попробуйте ещё раз.",
                    "Ошибка!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            // Setting the visuals back
            FilesListDataGrid.Enabled = true;
            FilesListDataGrid.CurrentCell = null;
            FilesListDataGrid.DataSource = null;
            FilesListDataGrid.Refresh();

            AboutButton.Enabled = true;
            BrowseFilesButton.Enabled = true;
            ConvertFilesButton.Enabled = false;

            SettingsButton.Enabled = true;
            splash.Close();

            BrowseFilesButton.Focus();
        }

        private void ButtonSettings_Click(object sender, EventArgs e)
        {
            SettingsForm settingsForm = new SettingsForm();
            settingsForm.ShowDialog();
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog();
        }

        #endregion

        #region DataGridHandlers

        private void FilesListDataGrid_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Back:
                case Keys.Delete:
                    if (FilesListDataGrid.SelectedCells.Count > 0)
                    {
                        // delete selected row(s)
                        foreach (DataGridViewCell cell in FilesListDataGrid.SelectedCells)
                        {
                            Controler.FilesToConvert.Remove((string)cell.Value);
                        }

                        // update DataGridView
                        if (Controler.FilesToConvert.Count > 0)
                        {
                            FilesListDataGrid.DataSource = Controler.FilesToConvert.Select(x => new { Value = x }).ToList();
                            FilesListDataGrid.ClearSelection();
                        }
                        else
                        {
                            FilesListDataGrid.DataSource = null;
                            FilesListDataGrid.Refresh();

                            FilesListDataGrid.Enabled = false;
                            ConvertFilesButton.Enabled = false;
                        }
                    }
                    break;

                case Keys.Escape:
                    FilesListDataGrid.ClearSelection();
                    break;
            }
        }

        #endregion
    }
}
