﻿using System.Windows.Forms;

namespace InvoiceToMMOConverter.Forms
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();

            LogoPictureBox.Image = Properties.Resources.refresh_icon.ToBitmap();

            VersionLabel.Text = string.Format("v. {0}", Properties.Settings.Default.app_version);

            this.Width = AppNameLabel.Right + 50;
            this.Height = CreditsLabel.Bottom + 50;
            this.MinimumSize = this.MaximumSize = new System.Drawing.Size(this.Width, this.Height);
        }
    }
}
