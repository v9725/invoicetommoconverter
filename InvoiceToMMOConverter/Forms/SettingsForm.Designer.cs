﻿namespace InvoiceToMMOConverter.Forms
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.СheckBoxDefaultDirs = new System.Windows.Forms.CheckBox();
            this.CheckBoxAutoMod = new System.Windows.Forms.CheckBox();
            this.ImportTextLabel = new System.Windows.Forms.Label();
            this.ExportTextLabel = new System.Windows.Forms.Label();
            this.ImportDirTextBox = new System.Windows.Forms.TextBox();
            this.ExportDirTextBox = new System.Windows.Forms.TextBox();
            this.ImportPathBrowseButton = new System.Windows.Forms.Button();
            this.ExportPathBrowseButton = new System.Windows.Forms.Button();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.LogDebugDataCheckBox = new System.Windows.Forms.CheckBox();
            this.CheckBoxDeleteAfter = new System.Windows.Forms.CheckBox();
            this.CheckUpdateButton = new System.Windows.Forms.Button();
            this.MainPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // СheckBoxDefaultDirs
            // 
            this.СheckBoxDefaultDirs.AutoSize = true;
            this.СheckBoxDefaultDirs.Checked = true;
            this.СheckBoxDefaultDirs.CheckState = System.Windows.Forms.CheckState.Checked;
            this.СheckBoxDefaultDirs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.СheckBoxDefaultDirs.Location = new System.Drawing.Point(6, 57);
            this.СheckBoxDefaultDirs.Name = "СheckBoxDefaultDirs";
            this.СheckBoxDefaultDirs.Size = new System.Drawing.Size(320, 22);
            this.СheckBoxDefaultDirs.TabIndex = 0;
            this.СheckBoxDefaultDirs.Text = "Использовать директории по умолчанию";
            this.СheckBoxDefaultDirs.UseVisualStyleBackColor = true;
            this.СheckBoxDefaultDirs.CheckedChanged += new System.EventHandler(this.СheckBoxDefaultDirs_CheckedChanged);
            // 
            // CheckBoxAutoMod
            // 
            this.CheckBoxAutoMod.AutoSize = true;
            this.CheckBoxAutoMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckBoxAutoMod.Location = new System.Drawing.Point(6, 3);
            this.CheckBoxAutoMod.Name = "CheckBoxAutoMod";
            this.CheckBoxAutoMod.Size = new System.Drawing.Size(195, 22);
            this.CheckBoxAutoMod.TabIndex = 1;
            this.CheckBoxAutoMod.Text = "Автоматический режим";
            this.CheckBoxAutoMod.UseVisualStyleBackColor = true;
            this.CheckBoxAutoMod.CheckedChanged += new System.EventHandler(this.CheckBoxAutoMod_CheckedChanged);
            // 
            // ImportTextLabel
            // 
            this.ImportTextLabel.AutoSize = true;
            this.ImportTextLabel.Enabled = false;
            this.ImportTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ImportTextLabel.Location = new System.Drawing.Point(3, 86);
            this.ImportTextLabel.Name = "ImportTextLabel";
            this.ImportTextLabel.Size = new System.Drawing.Size(66, 18);
            this.ImportTextLabel.TabIndex = 2;
            this.ImportTextLabel.Text = "Импорт:";
            // 
            // ExportTextLabel
            // 
            this.ExportTextLabel.AutoSize = true;
            this.ExportTextLabel.Enabled = false;
            this.ExportTextLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExportTextLabel.Location = new System.Drawing.Point(3, 114);
            this.ExportTextLabel.Name = "ExportTextLabel";
            this.ExportTextLabel.Size = new System.Drawing.Size(71, 18);
            this.ExportTextLabel.TabIndex = 3;
            this.ExportTextLabel.Text = "Экспорт:";
            // 
            // ImportDirTextBox
            // 
            this.ImportDirTextBox.Enabled = false;
            this.ImportDirTextBox.Location = new System.Drawing.Point(80, 85);
            this.ImportDirTextBox.Name = "ImportDirTextBox";
            this.ImportDirTextBox.ReadOnly = true;
            this.ImportDirTextBox.Size = new System.Drawing.Size(278, 22);
            this.ImportDirTextBox.TabIndex = 4;
            this.ImportDirTextBox.Text = "C:\\App\\import\\";
            // 
            // ExportDirTextBox
            // 
            this.ExportDirTextBox.Enabled = false;
            this.ExportDirTextBox.Location = new System.Drawing.Point(80, 114);
            this.ExportDirTextBox.Name = "ExportDirTextBox";
            this.ExportDirTextBox.ReadOnly = true;
            this.ExportDirTextBox.Size = new System.Drawing.Size(278, 22);
            this.ExportDirTextBox.TabIndex = 5;
            this.ExportDirTextBox.Text = "C:\\App\\export\\";
            // 
            // ImportPathBrowseButton
            // 
            this.ImportPathBrowseButton.Enabled = false;
            this.ImportPathBrowseButton.Location = new System.Drawing.Point(364, 85);
            this.ImportPathBrowseButton.Name = "ImportPathBrowseButton";
            this.ImportPathBrowseButton.Size = new System.Drawing.Size(86, 23);
            this.ImportPathBrowseButton.TabIndex = 6;
            this.ImportPathBrowseButton.Text = "Выбрать...";
            this.ImportPathBrowseButton.UseVisualStyleBackColor = true;
            this.ImportPathBrowseButton.Click += new System.EventHandler(this.ImportPathBrowseButton_Click);
            // 
            // ExportPathBrowseButton
            // 
            this.ExportPathBrowseButton.Enabled = false;
            this.ExportPathBrowseButton.Location = new System.Drawing.Point(364, 113);
            this.ExportPathBrowseButton.Name = "ExportPathBrowseButton";
            this.ExportPathBrowseButton.Size = new System.Drawing.Size(86, 23);
            this.ExportPathBrowseButton.TabIndex = 7;
            this.ExportPathBrowseButton.Text = "Выбрать...";
            this.ExportPathBrowseButton.UseVisualStyleBackColor = true;
            this.ExportPathBrowseButton.Click += new System.EventHandler(this.ExportPathBrowseButton_Click);
            // 
            // MainPanel
            // 
            this.MainPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainPanel.Controls.Add(this.LogDebugDataCheckBox);
            this.MainPanel.Controls.Add(this.CheckBoxDeleteAfter);
            this.MainPanel.Controls.Add(this.CheckUpdateButton);
            this.MainPanel.Controls.Add(this.СheckBoxDefaultDirs);
            this.MainPanel.Controls.Add(this.ExportPathBrowseButton);
            this.MainPanel.Controls.Add(this.CheckBoxAutoMod);
            this.MainPanel.Controls.Add(this.ImportPathBrowseButton);
            this.MainPanel.Controls.Add(this.ImportTextLabel);
            this.MainPanel.Controls.Add(this.ExportDirTextBox);
            this.MainPanel.Controls.Add(this.ExportTextLabel);
            this.MainPanel.Controls.Add(this.ImportDirTextBox);
            this.MainPanel.Location = new System.Drawing.Point(1, 1);
            this.MainPanel.Margin = new System.Windows.Forms.Padding(1);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(455, 220);
            this.MainPanel.TabIndex = 8;
            // 
            // LogDebugDataCheckBox
            // 
            this.LogDebugDataCheckBox.AutoSize = true;
            this.LogDebugDataCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogDebugDataCheckBox.Location = new System.Drawing.Point(6, 142);
            this.LogDebugDataCheckBox.Name = "LogDebugDataCheckBox";
            this.LogDebugDataCheckBox.Size = new System.Drawing.Size(260, 22);
            this.LogDebugDataCheckBox.TabIndex = 10;
            this.LogDebugDataCheckBox.Text = "Записывать отладочные данные";
            this.LogDebugDataCheckBox.UseVisualStyleBackColor = true;
            // 
            // CheckBoxDeleteAfter
            // 
            this.CheckBoxDeleteAfter.AutoSize = true;
            this.CheckBoxDeleteAfter.Checked = true;
            this.CheckBoxDeleteAfter.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckBoxDeleteAfter.Enabled = false;
            this.CheckBoxDeleteAfter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckBoxDeleteAfter.Location = new System.Drawing.Point(6, 29);
            this.CheckBoxDeleteAfter.Name = "CheckBoxDeleteAfter";
            this.CheckBoxDeleteAfter.Size = new System.Drawing.Size(391, 22);
            this.CheckBoxDeleteAfter.TabIndex = 9;
            this.CheckBoxDeleteAfter.Text = "Удалять оригиналы сконвертированных накладных";
            this.CheckBoxDeleteAfter.UseVisualStyleBackColor = true;
            this.CheckBoxDeleteAfter.CheckedChanged += new System.EventHandler(this.CheckBoxDeleteAfter_CheckedChanged);
            // 
            // CheckUpdateButton
            // 
            this.CheckUpdateButton.Enabled = false;
            this.CheckUpdateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CheckUpdateButton.Location = new System.Drawing.Point(125, 168);
            this.CheckUpdateButton.Name = "CheckUpdateButton";
            this.CheckUpdateButton.Size = new System.Drawing.Size(201, 45);
            this.CheckUpdateButton.TabIndex = 8;
            this.CheckUpdateButton.Text = "Проверить наличие\r\nобновлений";
            this.CheckUpdateButton.UseVisualStyleBackColor = true;
            this.CheckUpdateButton.Visible = false;
            this.CheckUpdateButton.Click += new System.EventHandler(this.CheckUpdateButton_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(457, 222);
            this.Controls.Add(this.MainPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройки приложения";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsForm_FormClosing);
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox СheckBoxDefaultDirs;
        private System.Windows.Forms.CheckBox CheckBoxAutoMod;
        private System.Windows.Forms.Label ImportTextLabel;
        private System.Windows.Forms.Label ExportTextLabel;
        private System.Windows.Forms.TextBox ImportDirTextBox;
        private System.Windows.Forms.TextBox ExportDirTextBox;
        private System.Windows.Forms.Button ImportPathBrowseButton;
        private System.Windows.Forms.Button ExportPathBrowseButton;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Button CheckUpdateButton;
        private System.Windows.Forms.CheckBox CheckBoxDeleteAfter;
        private System.Windows.Forms.CheckBox LogDebugDataCheckBox;
    }
}