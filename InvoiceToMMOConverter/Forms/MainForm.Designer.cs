﻿namespace InvoiceToMMOConverter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BrowseFilesButton = new System.Windows.Forms.Button();
            this.ConvertFilesButton = new System.Windows.Forms.Button();
            this.FilesListDataGrid = new System.Windows.Forms.DataGridView();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.AboutButton = new System.Windows.Forms.Button();
            this.ControlButtonsPannel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.FilesListDataGrid)).BeginInit();
            this.ControlButtonsPannel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BrowseFilesButton
            // 
            this.BrowseFilesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BrowseFilesButton.Location = new System.Drawing.Point(-1, -1);
            this.BrowseFilesButton.Margin = new System.Windows.Forms.Padding(1, 1, 3, 3);
            this.BrowseFilesButton.Name = "BrowseFilesButton";
            this.BrowseFilesButton.Size = new System.Drawing.Size(200, 47);
            this.BrowseFilesButton.TabIndex = 0;
            this.BrowseFilesButton.Text = "Выбрать файл(ы)";
            this.BrowseFilesButton.UseVisualStyleBackColor = true;
            this.BrowseFilesButton.Click += new System.EventHandler(this.ButtonBrowseFiles_Click);
            // 
            // ConvertFilesButton
            // 
            this.ConvertFilesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ConvertFilesButton.Location = new System.Drawing.Point(205, -1);
            this.ConvertFilesButton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.ConvertFilesButton.Name = "ConvertFilesButton";
            this.ConvertFilesButton.Size = new System.Drawing.Size(200, 47);
            this.ConvertFilesButton.TabIndex = 1;
            this.ConvertFilesButton.Text = "Конвертировать";
            this.ConvertFilesButton.UseVisualStyleBackColor = true;
            this.ConvertFilesButton.Click += new System.EventHandler(this.ButtonConvertFiles_Click);
            // 
            // FilesListDataGrid
            // 
            this.FilesListDataGrid.AllowUserToAddRows = false;
            this.FilesListDataGrid.AllowUserToDeleteRows = false;
            this.FilesListDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.FilesListDataGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.FilesListDataGrid.Location = new System.Drawing.Point(1, 54);
            this.FilesListDataGrid.Name = "FilesListDataGrid";
            this.FilesListDataGrid.ReadOnly = true;
            this.FilesListDataGrid.RowTemplate.Height = 24;
            this.FilesListDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.FilesListDataGrid.Size = new System.Drawing.Size(665, 482);
            this.FilesListDataGrid.TabIndex = 4;
            this.FilesListDataGrid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FilesListDataGrid_KeyUp);
            // 
            // SettingsButton
            // 
            this.SettingsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SettingsButton.Location = new System.Drawing.Point(411, -1);
            this.SettingsButton.Margin = new System.Windows.Forms.Padding(3, 1, 3, 3);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(200, 47);
            this.SettingsButton.TabIndex = 2;
            this.SettingsButton.Text = "Настройки";
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.ButtonSettings_Click);
            // 
            // AboutButton
            // 
            this.AboutButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AboutButton.Location = new System.Drawing.Point(617, -2);
            this.AboutButton.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.AboutButton.Name = "AboutButton";
            this.AboutButton.Size = new System.Drawing.Size(47, 47);
            this.AboutButton.TabIndex = 3;
            this.AboutButton.Text = "?";
            this.AboutButton.UseVisualStyleBackColor = true;
            this.AboutButton.Click += new System.EventHandler(this.AboutButton_Click);
            // 
            // ControlButtonsPannel
            // 
            this.ControlButtonsPannel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ControlButtonsPannel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ControlButtonsPannel.Controls.Add(this.BrowseFilesButton);
            this.ControlButtonsPannel.Controls.Add(this.AboutButton);
            this.ControlButtonsPannel.Controls.Add(this.ConvertFilesButton);
            this.ControlButtonsPannel.Controls.Add(this.SettingsButton);
            this.ControlButtonsPannel.Location = new System.Drawing.Point(1, 1);
            this.ControlButtonsPannel.Margin = new System.Windows.Forms.Padding(0);
            this.ControlButtonsPannel.Name = "ControlButtonsPannel";
            this.ControlButtonsPannel.Size = new System.Drawing.Size(665, 51);
            this.ControlButtonsPannel.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(672, 544);
            this.Controls.Add(this.ControlButtonsPannel);
            this.Controls.Add(this.FilesListDataGrid);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Конвертер Расходных накладных в ММО";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FilesListDataGrid)).EndInit();
            this.ControlButtonsPannel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BrowseFilesButton;
        private System.Windows.Forms.Button ConvertFilesButton;
        private System.Windows.Forms.DataGridView FilesListDataGrid;
        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.Button AboutButton;
        private System.Windows.Forms.Panel ControlButtonsPannel;
    }
}

