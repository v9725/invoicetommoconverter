﻿using System;
using System.Windows.Forms;

using InvoiceToMMOConverter.Properties;

//using AutoUpdaterDotNET;

namespace InvoiceToMMOConverter.Forms
{
    public partial class SettingsForm : Form
    {
        #region Fields

        /// <summary>
        /// Automatic mode Description ToolTip.
        /// </summary>
        private string automodToolTipText =
            "Автоматически производит конвертацию файлов в директории \n" +
            "импорта и сохраняет в директорию экспорта. Для данного режима работы \n" +
            "необходимо, чтобы директория импорта содержала файлы накладных. В противном \n"+
            "случае, приложение запустится в ручном режиме (режиме по умолчанию)";

        /// <summary>
        /// Automod flag state as of opening the settings window.
        /// </summary>
        private bool autoModCurrentState;

        /// <summary>
        /// "Delete after converting" flag state as of opening the settings window.
        /// </summary>
        private bool deleteAfterCurrentState;

        /// <summary>
        /// Default Directories flag state as of opening the settings windows.
        /// </summary>
        private bool useDefDirCurrentState;

        /// <summary>
        /// "Log debug data" flag state as of opening settings window.
        /// </summary>
        private bool logDebugDataCurrentState;

        /// <summary>
        /// Current import directory.
        /// </summary>
        private string importDirCurrentState;

        /// <summary>
        /// Current export directory.
        /// </summary>
        private string exportDirCurrentState;

        #endregion

        #region Constructor and Closing Event

        /// <summary>
        /// Form Contructor.
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();

            // Applying State
            CheckBoxAutoMod.Checked = autoModCurrentState = Settings.Default.auto_mod;
            СheckBoxDefaultDirs.Checked = useDefDirCurrentState = Settings.Default.use_def_dir;
            CheckBoxDeleteAfter.Checked = deleteAfterCurrentState = Settings.Default.delete_after;
            LogDebugDataCheckBox.Checked = logDebugDataCurrentState = Settings.Default.log_debug;

            importDirCurrentState = ImportDirTextBox.Text = Settings.Default.import_dir;
            exportDirCurrentState = ExportDirTextBox.Text = Settings.Default.export_dir;

            // Visuals
            this.Icon = Resources.settings_icon;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            ToolTip autoModTT = new ToolTip { AutoPopDelay = 15000 };
            autoModTT.SetToolTip(CheckBoxAutoMod, automodToolTipText);
        }

        /// <summary>
        /// Saves any changed settings on Form closing.
        /// </summary>
        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool settingsChanged = false;

            // Looking if user chnaged setting from the previous values
            if (СheckBoxDefaultDirs.Checked != useDefDirCurrentState)
            {
                Settings.Default.use_def_dir = СheckBoxDefaultDirs.Checked;
                settingsChanged = true;
            }
            if(CheckBoxAutoMod.Checked != autoModCurrentState)
            {
                Settings.Default.auto_mod = CheckBoxAutoMod.Checked;
                settingsChanged = true;
            }
            if(CheckBoxDeleteAfter.Checked != deleteAfterCurrentState)
            {
                Settings.Default.delete_after = CheckBoxDeleteAfter.Checked;
                settingsChanged = true;
            }
            if (ImportDirTextBox.Text != importDirCurrentState)
            {
                Settings.Default.import_dir = ImportDirTextBox.Text;
                settingsChanged = true;
            }
            if (ExportDirTextBox.Text != exportDirCurrentState)
            {
                Settings.Default.export_dir = ExportDirTextBox.Text;
                settingsChanged = true;
            }
            if (LogDebugDataCheckBox.Checked != logDebugDataCurrentState)
            {
                Settings.Default.log_debug = LogDebugDataCheckBox.Checked;
                settingsChanged = true;
            }

            // Saving is they did
            if(settingsChanged)
            { Settings.Default.Save(); }
        }

        #endregion

        #region Controls Event Handlers

        /// <summary>
        /// Changes depending controls according to default dirs flag state.
        /// </summary>
        private void СheckBoxDefaultDirs_CheckedChanged(object sender, EventArgs e)
        {
            ImportTextLabel.Enabled = !СheckBoxDefaultDirs.Checked;
            ImportDirTextBox.Enabled = !СheckBoxDefaultDirs.Checked;
            ImportPathBrowseButton.Enabled = !СheckBoxDefaultDirs.Checked;

            ExportTextLabel.Enabled = !СheckBoxDefaultDirs.Checked;
            ExportDirTextBox.Enabled = !СheckBoxDefaultDirs.Checked;
            ExportPathBrowseButton.Enabled = !СheckBoxDefaultDirs.Checked;
        }

        /// <summary>
        /// Chnages depending control according to auto mode flag state.
        /// </summary>
        private void CheckBoxAutoMod_CheckedChanged(object sender, EventArgs e)
        {
            CheckBoxDeleteAfter.Enabled = CheckBoxAutoMod.Checked;
        }

        /// <summary>
        /// 
        /// </summary>
        private void CheckBoxDeleteAfter_CheckedChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Initiates new directory choice dialog for choosing new import directory.
        /// </summary>
        private void ImportPathBrowseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            if(folderDlg.ShowDialog() != DialogResult.Cancel)
            {
                ImportDirTextBox.Text = folderDlg.SelectedPath;
            }
        }

        /// <summary>
        /// Initiates new directory choice dialog for choosing new export directory.
        /// </summary>
        private void ExportPathBrowseButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDlg = new FolderBrowserDialog();
            if (folderDlg.ShowDialog() != DialogResult.Cancel)
            {
                ExportDirTextBox.Text = folderDlg.SelectedPath;
            }
        }

        /// <summary>
        /// Checks if there's newer version avaliable and if there is -- initiates the update.
        /// </summary>
        private void CheckUpdateButton_Click(object sender, EventArgs e)
        {
            //AutoUpdater.ShowSkipButton = false;
            //AutoUpdater.ShowRemindLaterButton = false;
            //AutoUpdater.InstalledVersion = new Version(Settings.Default.app_version);
            //AutoUpdater.Start("https://bitbucket.org/v9725/invoicetommo_releses/src/master/UpdateInfo.xml");
        }

        #endregion


    }
}
