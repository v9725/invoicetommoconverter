﻿
namespace InvoiceToMMOConverter.Logic
{
    public interface IXlsImporter
    {
        bool ImportFromFile(
            System.Collections.Generic.List<string> path, 
            ref System.Collections.Generic.List<System.Data.DataSet> FileContent
            );
    }
}
