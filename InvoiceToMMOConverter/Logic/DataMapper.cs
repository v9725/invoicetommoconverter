﻿using System;
using System.Collections.Generic;

using InvoiceToMMOConverter.Data;

using log4net;

namespace InvoiceToMMOConverter.Logic
{
    /// <summary>
    /// Static converter class for DataSet -> MMOEntry conversion.
    /// </summary>
    public static class DataMapper
    {
        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(DataMapper));

        public static Data.MMOEntry ConvertToMMO(System.Data.DataSet fileToConvert)
        {
            Data.MMOEntry resultEntry = new Data.MMOEntry();

            // Data to work with
            var RowsRange = fileToConvert.Tables[0].Rows;

            #region MetaData Init

            //  Invoice header components
            string[] fileHeader = null;


            // Reciever Name
            string recieveerName = "";

            // Reciever Addres Store Code
            int recieverAddresCode = -1;


            // Sum w/o Taxes (НДС)
            string sum = "";

            // Sum with Taxes (НДС)
            string sumTax = "";


            // Supplies Count
            int suppliesCount = -1;

            // Supplies Table starting index in the DataSet
            int suppliesTableStartRow = -1;

            #endregion

            #region Main Parcing Cycle

            // Flags for different parsing stages
            bool headerReady        = false;
            bool supplierReady      = false;
            bool receiverCodeReady  = false;
            bool recieverReady      = false;
            bool contentOntheLeft   = false;

            log.Info("Started parsing file \"" + fileToConvert.DataSetName +"\"...");

            // Looking through (almost) the entire DataSet
            for (int row = 2; row < fileToConvert.Tables[0].Rows.Count; row++)
            {
                for (int col = 1; col < fileToConvert.Tables[0].Columns.Count; col++)
                {
                    // if the cell is empty...
                    if (RowsRange[row][col].ToString() == "")
                    {
                        // ...and if it's before the table, then skip the entire row
                        if ((col == 1) && !contentOntheLeft)
                        { break; }
                        else
                        { continue; }
                    }
                    // ...or has unneeded data
                    else if (RowsRange[row][col].ToString().Contains("Договір:") ||
                             RowsRange[row][col].ToString().Contains("Склад:") ||
                             RowsRange[row][col].ToString().Contains("Розр. док.:") ||
                             RowsRange[row][col].ToString().Contains("Сума ПДВ:"))
                    { break; } // skip the row
                    else
                    {
                        // Looking at header
                        if (!headerReady)
                        {
                            // Invoice header components
                            if (RowsRange[row][col].ToString().Contains("Видаткова накладна"))
                            {
                                if (Properties.Settings.Default.log_debug)
                                { log.Debug("Header detected."); }

                                // Invoice header components
                                fileHeader = RowsRange[row][col].ToString().Split(' ');

                                headerReady = true;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        // Looking at Supplier
                        if (!supplierReady)
                        {
                            // Supplier Name and Requisites Components
                            if (RowsRange[row][col].ToString().Contains("Постачальник:"))
                            {
                                if (Properties.Settings.Default.log_debug)
                                { log.Debug("Supplier detected."); }

                                supplierReady = true;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        // Looking at Reciever 
                        if (!recieverReady)
                        {
                            // Reciever Name
                            if (RowsRange[row][col].ToString().Contains("Покупець:"))
                            {
                                if (Properties.Settings.Default.log_debug)
                                { log.Debug("Reciever detected."); }

                                // Reciever Name
                                recieveerName = RowsRange[row][col + 5].ToString();
                                recieveerName = recieveerName.Substring(
                                    recieveerName.IndexOf('"') + 1,
                                    recieveerName.LastIndexOf('"') - recieveerName.IndexOf('"') - 1
                                    );

                                recieverReady = true;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        if (!receiverCodeReady)
                        {
                            if (RowsRange[row][col].ToString().Contains("Адреса доставки:"))
                            {
                                if (Properties.Settings.Default.log_debug)
                                { log.Debug("Address code detected."); }

                                // Reciever Addres Store Code
                                recieverAddresCode = int.Parse(RowsRange[row][col + 5].ToString());

                                receiverCodeReady = true;
                                break;
                            }
                        }

                        // Looking at the rest of the Set

                        // Loooking for the supplies table start
                        if (suppliesTableStartRow == -1)
                        {
                            if(!receiverCodeReady)
                            {
                                log.Warn("WARNING! No Addess code detected!");
                            }
                            
                            // Marking table start
                            if (RowsRange[row][col].ToString() == "№")
                            {
                                if (Properties.Settings.Default.log_debug)
                                { log.Debug("Supply table detected. Skipping for now."); }

                                suppliesTableStartRow = row + 1;
                                contentOntheLeft = true;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }

                        // Sum w/o Taxes (НДС)
                        if (RowsRange[row][col].ToString().Contains("Всього:"))
                        {
                            // Sum w/o Taxes (НДС)
                            sum = RowsRange[row][col + 1].ToString().Replace(',', '.');
                            break;
                        }

                        // Sum with Taxes (НДС)
                        if (RowsRange[row][col].ToString().Contains("Всього із ПДВ:"))
                        {
                            // Sum with Taxes (НДС)
                            sumTax = RowsRange[row][col + 1].ToString().Replace(',', '.');
                            contentOntheLeft = false;
                            break;
                        }

                        // Supplies Count
                        if (RowsRange[row][col].ToString().Contains("Всього найменувань"))
                        {
                            // Supplies Count
                            int.TryParse(
                                   RowsRange[row][col].ToString().Split(',')[0].Split(' ')[2],
                                   out suppliesCount);
                            break;
                        }
                    }
                }
            }

            log.Info("Finished parsing.");

            #endregion

            #region MetaData Calc

            // Document Date
            DateTime docDate = new DateTime(
                int.Parse(fileHeader[7]),                       // Year
                int.Parse(GetMonthNumberByName(fileHeader[6])), // Month
                int.Parse(fileHeader[5])                        // Day
                );

            // File Name
            try
            {
                resultEntry.Name = string.Format("{0}_{1}__{2}_{3}__{4}",
                    docDate.ToString("ddMMyyyy"),                                       // Conversion Date
                    DateTime.Now.ToString("hhmmss"),                                    // Conversion Time
                    fileHeader[3],                                                      // Doc Number
                    recieveerName,                                                      // Reciever
                    MetadataStorage.Clients[recieveerName].Stores[recieverAddresCode]   //Address 
                    );
            }
            catch(KeyNotFoundException)
            {
                System.Windows.Forms.MessageBox.Show(
                    "Точка отсутствует в перечне известных точек!\n"+
                    "Программа пропустит этот файл!\n\n" + 
                    "Код точки: " + recieverAddresCode + "\n" + 
                    "Номер накладной: " + fileHeader[3], 
                    
                    "ОШИБКА!", 
                    System.Windows.Forms.MessageBoxButtons.OK, 
                    System.Windows.Forms.MessageBoxIcon.Error
                    );

                log.Error(
                    "Error!\n" +
                    "\t\t\t\tТочка отсутствует в перечне известных точек!\n" +
                    "\t\t\t\tПропускаю файл!\n" +
                    "\t\t\t\tКод точки: " + recieverAddresCode + "\n" +
                    "\t\t\t\tНомер накладной: " + fileHeader[3]
                    );

                return null;
            }
            #endregion

            #region MMO Construction

            log.Info("Started constructing...");

            #region Sector 1

            // Sector 1
            resultEntry.Sector1_Header.Add("РАСХОДНАЯ_НАКЛАДНАЯ");
            resultEntry.Sector1_Header.Add(SupplierInfo.SupplierCode); // ЄДРПОУ поставщика
            resultEntry.Sector1_Header.Add(MetadataStorage.Clients[recieveerName].ClientID.ToString()); // ЄДРПОУ аптеки-получателя
            resultEntry.Sector1_Header.Add("версия_3");

            if (Properties.Settings.Default.log_debug)
            { log.Debug("Sector 1 constructed."); }

            #endregion

            #region Sector 2

            #region Номер документа
            // 01
            resultEntry.Sector2_Requisites.Add(fileHeader[3]);
            #endregion

            #region Дата документа
            // 02
            resultEntry.Sector2_Requisites.Add(docDate.ToString("dd.MM.yyyy"));
            #endregion

            #region Номер налоговой накладной
            // 02   Пока пустой
            resultEntry.Sector2_Requisites.Add(" ");
            #endregion

            #region Наименование поставщика
            // 03
            resultEntry.Sector2_Requisites.Add(SupplierInfo.Name);
            #endregion

            #region Банк
            // 04 
            resultEntry.Sector2_Requisites.Add(SupplierInfo.SupplierBank);
            //resultEntry.Sector2_Requisites.Add(supplierRequisites[0].Substring(supplierRequisites[0].LastIndexOf('у') + 2));
            #endregion

            #region Р/с
            // 05
            resultEntry.Sector2_Requisites.Add(SupplierInfo.IBAN);
            //resultEntry.Sector2_Requisites.Add(supplierRequisites[0].Substring(supplierRequisites[0].IndexOf("п/р") + 4, 29));
            #endregion

            #region МФО
            // 06
            resultEntry.Sector2_Requisites.Add(SupplierInfo.MFO);
            #endregion

            #region Телефон(ы)
            // 07
            resultEntry.Sector2_Requisites.Add(SupplierInfo.PhoneNumber);
            //resultEntry.Sector2_Requisites.Add(supplierRequisites[10].Substring(supplierRequisites[10].IndexOf(':') + 2));
            #endregion

            #region Адрес поставщика
            // 08
            resultEntry.Sector2_Requisites.Add(SupplierInfo.SupplierAddress);
            //resultEntry.Sector2_Requisites.Add(
            //    string.Format(
            //        "{0},{1},{2},{3},{4},{5}",
            //        supplierRequisites[7].TrimStart(' '),
            //        supplierRequisites[8],
            //        supplierRequisites[9],
            //        supplierRequisites[5],
            //        supplierRequisites[4],
            //        supplierRequisites[3]
            //    ));
            #endregion

            #region Номер лицензии
            // 09
            resultEntry.Sector2_Requisites.Add(" ");
            #endregion

            #region Срок лицензии
            // 10
            resultEntry.Sector2_Requisites.Add(" ");
            #endregion

            #region Сумма без НДС
            // 11
            resultEntry.Sector2_Requisites.Add(sum);

            #endregion

            #region Сумма с НДС
            // 12
            resultEntry.Sector2_Requisites.Add(sumTax);
            #endregion

            #region Срок оплаты
            // 13
            resultEntry.Sector2_Requisites.Add(" ");
            #endregion

            #region Признак: цена включает НДС или не вкл.НДС
            // 14
            resultEntry.Sector2_Requisites.Add("1");
            #endregion

            #region Код метода синхронизации
            // 15
            resultEntry.Sector2_Requisites.Add("1");
            #endregion

            #region Число значащих цифр дробной части реквизита "Сумма отпускная" в строках документа
            // 16
            resultEntry.Sector2_Requisites.Add("4");
            #endregion

            if (Properties.Settings.Default.log_debug)
            { log.Debug("Sector 2 constructed."); }

            #endregion

            #region Sector 3

            // Sector 3
            resultEntry.Sector3_Commentary = recieverAddresCode.ToString();

            if (Properties.Settings.Default.log_debug)
            { log.Debug("Sector 3 constructed."); }

            #endregion

            #region Sector 4

            // Parsing the supplies table
            for (int row = suppliesTableStartRow, i = 0; i < suppliesCount; i++, row++)
            {
                #region Local Meta-data

                // Supply Name
                string supplyName = RowsRange[row][3].ToString();

                // Price with Taxes (ПДВ)
                string priceTaxString = (Convert.ToDouble(
                    RowsRange[row][29].ToString()) * 1.2 // 20% ПДВ
                    ).ToString(
                        new System.Globalization.NumberFormatInfo()
                        { NumberDecimalSeparator = "." }
                        );

                // Expiration Date
                string expirationDateString = docDate.AddMonths(6).ToString("dd.MM.yyyy");

                #endregion

                // Supplie Row
                List<string> supplyRow = new List<string>(23)
                {
                    SupplierInfo.SuppliesCodes[supplyName],     // ID товара
                    supplyName,                                 // Наименование
                    SupplierInfo.SupplierCode,                  // ID производителя (ЕРДПОУ)
                    SupplierInfo.Name,                          // Наименование производителя
                    " ",                                        // ID внешний (например Морион) 
                    " ",                                        // Номер регистрации
                    " ",                                        // Дата регистрации
                    " ",                                        // Срок регистрации
                    "20",                                       // Процент НДС 20
                    " ",                                        // Макс процент от тамож
                    " ",                                        // Номер Серии
                    " ",                                        // Номер сертификата
                    " ",                                        // Дата сертификата
                    expirationDateString,                       // Срок годности (+ 6 месяцев)
                    RowsRange[row][26].ToString(),              // Единицы Измерения
                    RowsRange[row][21].ToString(),              // Количество
                    " ",                                        // Цена Складская
                    " ",                                        // Цена Таможенная
                    " ",                                        // Процент наценки поставщика
                    priceTaxString,                             // Цена Отпускная (для аптеки это закупочная)
                    sumTax,                                     // Сумма отпускная
                    SupplierInfo.SupplyCategoryCode,            // код УКТЗЕД
                    SupplierInfo.SuppliesCodes[supplyName]      // штрих-код (ID товара опять)
                };

                // Adding the row to the list
                resultEntry.Sector4_ItemsList.Add(supplyRow);
            }

            if (Properties.Settings.Default.log_debug)
            { log.Debug("Sector 4 constructed."); }

            #endregion

            log.Info("File constructed.");

            #endregion

            return resultEntry;
        }

        /// <summary>
        /// Convert Ukrainian month name to its number
        /// </summary>
        /// <param name="monthNameUA">Ukrainian month name.</param>
        /// <returns>Month number</returns>
        private static string GetMonthNumberByName(string monthNameUA)
        {
            switch (monthNameUA)
            {
                case "січня":
                    {
                        return "01";
                    }
                case "лютого":
                    {
                        return "02";
                    }
                case "березня":
                    {
                        return "03";
                    }
                case "квітня":
                    {
                        return "04";
                    }
                case "травня":
                    {
                        return "05";
                    }
                case "червня":
                    {
                        return "06";
                    }
                case "липня":
                    {
                        return "07";
                    }
                case "серпня":
                    {
                        return "08";
                    }
                case "вересня":
                    {
                        return "09";
                    }
                case "жовтня":
                    {
                        return "10";
                    }
                case "листопада":
                    {
                        return "11";
                    }
                case "грудня":
                    {
                        return "12";
                    }
                default:
                    {
                        return null;
                    }
            }
        }
    }
}
