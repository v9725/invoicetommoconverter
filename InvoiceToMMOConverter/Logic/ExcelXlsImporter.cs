﻿using System;
using System.Collections.Generic;
using System.Data;

using log4net;

using Excel = Microsoft.Office.Interop.Excel;
using Marshal = System.Runtime.InteropServices.Marshal;

namespace InvoiceToMMOConverter.Logic
{
    /// <summary>
    /// Класс, осуществляющий взаимодействие с XLS-таблицами через скрытый экземпляр Microsoft Excel.
    /// </summary>
    public class ExcelXlsImporter : IXlsImporter
    {
        #region Fields and Properties

        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(ExcelXlsImporter));

        /// <summary>
        /// Ссылка на экземпляр приложения Excel.
        /// </summary>
        private Excel.Application _excelApp = null;
        
        /// <summary>
        /// Ссылка на рабочую книгу.
        /// </summary>
        private Excel.Workbook _xWorkBook = null;

        /// <summary>
        /// Служебный флаг состояния Excel-приложения.
        /// </summary>
        private bool _isOpened = false;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        public ExcelXlsImporter()
        { }

        #endregion

        #region FileDumping Method

        /// <summary>
        /// Reads excel workbooks located in specified paths and dumps their content into a List of DataSets.
        /// </summary>
        /// <param name="paths">Full file paths.</param>
        /// <returns>Wether or not operation was successful.</returns>
        public bool ImportFromFile(List<string> paths, ref List<DataSet> FilesContent)
        {
            try
            {
                log.Info("Initializing files uploading...");
                
                // Initializing Excel Instance.
                _excelApp = new Excel.Application();

                // For every file
                foreach (string path in paths)
                {
                    log.Info("Started uploading file \"" + path + "\"");

                    // Initializing resulting DataSet
                    DataSet ds = new DataSet(path);

                    // Closing previous file if it's opened
                    if (_isOpened) { _xWorkBook.Close(); }

                    // Opening current file
                    _xWorkBook = _excelApp.Workbooks.Open(
                        path,
                        Type.Missing,
                        true,
                        Type.Missing,
                        Type.Missing,
                        Type.Missing,
                        Type.Missing,
                        Type.Missing,
                        Type.Missing,
                        false);
                    _isOpened = true;

                    // For every sheet in a file
                    foreach (Excel.Worksheet sheet in _xWorkBook.Worksheets)
                    {
                        // initializing storage
                        DataTable dt = new DataTable(sheet.Name);

                        // Getting the boundaries
                        int rows = sheet.UsedRange.Rows.Count;
                        int cols = sheet.UsedRange.Columns.Count;


                        // Naming the coumns (1, 2, 3,...)
                        for (int iCols = 1; iCols <= cols; iCols++)
                        {
                            dt.Columns.Add(iCols.ToString());
                        }

                        // End of useful info flag
                        bool EOF = false;

                        // Per-cell data copying
                        for (int iRows = 1; iRows <= rows; iRows++)
                        {
                            if (!EOF)
                            {
                                DataRow dr = dt.NewRow();
                                for (int iCols = 1; iCols <= cols; iCols++)
                                {
                                    // Copying cell value
                                    dr[iCols - 1] = sheet.Cells[iRows, iCols].Value;

                                    // Checking for the end of useful info
                                    if (dr[iCols - 1].ToString().StartsWith("Всього найменувань"))
                                    {
                                        if (Properties.Settings.Default.log_debug)
                                        { log.Debug("Reached end of file."); }
                                        
                                        EOF = true;
                                        break;
                                    }
                                }

                                // Adding a row into the table
                                dt.Rows.Add(dr);
                            }
                            else
                                break;
                        }
                        
                        // Adding a table into the set
                        ds.Tables.Add(dt);
                    }

                    // Adding a set into the list
                    FilesContent.Add(ds);
                }

                log.Info("Successfully uploaded " + paths.Count + " files.");
                return true;
            }
            catch (Exception exc)
            {
                log.Error("Error occured during file importing.", exc);
                System.Windows.Forms.MessageBox.Show(
                    exc.Message +
                    "\n=================================\n" +
                    exc.StackTrace
                );

                return false;
            }
            finally
            {
                // Closing and cleaning the COM objects
                CleanUp();
            }
        }

        /// <summary>
        /// Closs and Marshals COM objects.
        /// </summary>
        private void CleanUp()
        {
            if (_isOpened)
            {
                if (Properties.Settings.Default.log_debug)
                { log.Debug("Started cleaning up after uploading."); }
                

                // Collecting the garbage
                GC.Collect();
                GC.WaitForPendingFinalizers();

                // Cleaning the book
                _xWorkBook.Close(false);
                Marshal.ReleaseComObject(_xWorkBook);

                // Cleaning the app
                _excelApp.Quit();
                Marshal.ReleaseComObject(_excelApp);

                // "Not opened anymore"
                _isOpened = false;

                if (Properties.Settings.Default.log_debug)
                { log.Debug("Finished cleaning up."); }
                
            }
        }

        #endregion
    }
}
