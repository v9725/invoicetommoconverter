﻿using System;
using System.Collections.Generic;
using System.Data;

using log4net;

using ExcelDataReader;

namespace InvoiceToMMOConverter.Logic
{
    /// <summary>
    /// Класс, осуществляющий взаимодействие с XLS-таблицами.
    /// </summary>
    public class XlsImporter : IXlsImporter
    {
        #region Fields and Properties

        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(XlsImporter));

        #endregion

        public XlsImporter()
        { }

        #region FileDumping Method

        /// <summary>
        /// Reads excel workbooks located in specified paths and dumps their content into a List of DataSets.
        /// </summary>
        /// <param name="paths">Full file paths.</param>
        /// <returns>Wether or not operation was successful.</returns>
        public bool ImportFromFile(List<string> paths, ref List<DataSet> FilesContent)
        {
            try
            {
                log.Info("Initializing files uploading...");

                // For every file
                foreach (string path in paths)
                {
                    log.Info("Started uploading file \"" + path + "\"");

                    using (var stream = System.IO.File.Open(path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
                    {
                        // Auto-detect format, supports:
                        //  - Binary Excel files (2.0-2003 format; *.xls)
                        //  - OpenXml Excel files (2007 format; *.xlsx, *.xlsb)
                        using (var reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            // Uses the AsDataSet extension method
                            DataSet result = reader.AsDataSet();
                            result.DataSetName = path;
                            FilesContent.Add(result);
                            // The result of each spreadsheet is in result.Tables
                        }
                    }
                }

                log.Info("Successfully uploaded " + paths.Count + " files.");
                return true;
            }
            catch(Exception exc)
            {
                log.Error("Error occured during file importing.", exc);
                System.Windows.Forms.MessageBox.Show(
                    exc.Message +
                    "\n=================================\n" +
                    exc.StackTrace
                );

                return false;
            }
        }

            #endregion
        }
}
