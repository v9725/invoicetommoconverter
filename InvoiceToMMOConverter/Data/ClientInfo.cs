﻿using System.Collections.Generic;

namespace InvoiceToMMOConverter.Data
{
    /// <summary>
    /// Structure with the information about the client.
    /// </summary>
    public class ClientInfo
    {
        #region Fields

        /// <summary>
        /// Нименование заказчика.
        /// </summary>
        public string Name { get; } = "";

        /// <summary>
        /// ЕДРПОУ заказчика.
        /// </summary>
        public int ClientID { get; } = -1;

        /// <summary>
        /// Коды аптек по адресам.
        /// </summary>
        public Dictionary<int, string> Stores { get; } = null;

        #endregion

        #region Constructor

        /// <summary>
        /// Конструктор класса.
        /// </summary>
        /// <param name="name">Наименование заказчикаю</param>
        /// <param name="id">ЕДРПОУ заказчика.</param>
        /// <param name="stores">Коды аптек по адресам.</param>
        public ClientInfo(string name, int id, Dictionary<int, string> stores)
        {
            this.Name = name;
            this.ClientID = id;
            this.Stores = stores;
        }

        #endregion
    }
}
