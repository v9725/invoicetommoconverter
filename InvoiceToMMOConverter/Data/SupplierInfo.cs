﻿using System;
using System.Collections.Generic;

namespace InvoiceToMMOConverter.Data
{
    /// <summary>
    /// Structure with the information about the supplier.
    /// </summary>
    public static class SupplierInfo
    {
        #region Fields

        /// <summary>
        /// Наименование поставщика.
        /// </summary>
        public static readonly string Name = "ТОВ «АРНІКО ТРЕЙД»";

        /// <summary>
        /// Коды товаров
        /// </summary>
        public static readonly Dictionary<string, string> SuppliesCodes = new Dictionary<string, string>()
        {
            { "Вода питна негазована",      "001" },
            { "Вода питна негазована 0,5 л", "4820212910012" },
            { "Вода питна негазована 1 л",   "4820212910029" },
            { "Вода питна негазована 1,5 л", "4820212910043" },
            { "Вода питна негазована 2 л",   "4820212910050" },
            { "Вода питна негазована  5л",   "4820212910067" },
            { "Вода питна негазована 10 л",  "4820212910074" },
        };

        /// <summary>
        /// МФО
        /// </summary>
        public static readonly string MFO = "300528";

        /// <summary>
        /// ЕДРПОУ поставщика
        /// </summary>
        public static readonly string SupplierCode = "40599704";

        /// <summary>
        /// код УКТЗЕД
        /// </summary>
        public static readonly string SupplyCategoryCode = "2201 90 00 00";

        //п/р UA693005280000026006455036249 у банку ПАТ "ОТП БАНК", м.Київ,
        //УКРАЇНА, 61012, Харківська обл., м.Харків, Ленінський р-н, вул.Червоні ряди, буд. 14, кімн. 209, тел.: 0957825392,
        //код за ЄДРПОУ 40599704, ІПН 405997020339

        /// <summary>
        /// Адресс поставщика.
        /// </summary>
        public static readonly string SupplierAddress = "вул. Червоні ряди, буд. 14, кімн. 209, м. Харків, Харківська обл., 61012";

        /// <summary>
        /// Банк поставщика.
        /// </summary>
        public static readonly string SupplierBank = "ПАТ \"ОТП БАНК\", м.Київ";

        /// <summary>
        /// Расчётный счёт.
        /// </summary>
        public static readonly string IBAN = "UA693005280000026006455036249";

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public static readonly string PhoneNumber = "0957825392";

        #endregion
    }
}
