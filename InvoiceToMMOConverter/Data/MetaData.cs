﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using log4net;

namespace InvoiceToMMOConverter.Data
{
    /// <summary>
    /// Statis storage of meta-data about supplier and clients.
    /// </summary>
    public static class MetadataStorage
    {
        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(MetadataStorage));

        /// <summary>
        /// Перечень заказчиков и их данных.
        /// </summary>
        public static Dictionary<string, ClientInfo> Clients = new Dictionary<string, ClientInfo>();


        public static bool LoadClients()
        {
            if (Properties.Settings.Default.log_debug)
            { log.Debug("Initialized loading clients info..."); }

            //Grabbing files from the folder
            string[] clientsInfo = Directory.GetFiles(
                Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "data", "clients")
                );

            //If no clients
            if(clientsInfo.Length == 0)
            {
                throw new Exception("NO LOADED CLIENTINFO FILES DETECTED!");
            }
            else
            {
                if (Properties.Settings.Default.log_debug)
                { log.Debug("Detected " + clientsInfo.Length + " files. Starting parsing..."); }
            }

            try
            {
                //Parse each one
                foreach (string clientpath in clientsInfo)
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(clientpath);
                    {
                        if (Properties.Settings.Default.log_debug)
                        { log.Debug("Started parsing \"" + Path.GetFileName(clientpath) + "\""); }

                        var nodePointer = xmlDoc.DocumentElement.SelectSingleNode("/Client/Name");
                        string clientName = nodePointer.InnerText;

                        nodePointer = xmlDoc.DocumentElement.SelectSingleNode("/Client/ClientID");
                        int clientID = int.Parse(nodePointer.InnerText);

                        Dictionary<int, string> storeCodes = new Dictionary<int, string>();

                        foreach (XmlNode node in xmlDoc.DocumentElement.SelectNodes("/Client/Stores/Store"))
                        {
                            storeCodes.Add(int.Parse(node.FirstChild.InnerText), node.LastChild.InnerText);
                        }

                        ClientInfo info = new ClientInfo(clientName, clientID, storeCodes);
                        Clients.Add(clientName, info);
                    }
                }

                log.Info("Client info loaded successfully!");
                return true;
            }
            catch (Exception exc)
            {
                log.Error("ERROR!", exc);
                return false;
            }
        }
    }
}
