﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

using log4net;

using InvoiceToMMOConverter.Data;
using InvoiceToMMOConverter.Logic;

namespace InvoiceToMMOConverter
{
    /// <summary>
    /// Class that controls data exchange between data layer and GUI layer.
    /// </summary>
    public class Controler
    {
        #region Fields

        /// <summary>
        /// Logger.
        /// </summary>
        private static readonly ILog log = LogManager.GetLogger(typeof(Controler));

        /// <summary>
        /// XLS files importer.
        /// </summary>
        private IXlsImporter Importer;

        /// <summary>
        /// List of uploaded files.
        /// </summary>
        public List<string> FilesToConvert;

        /// <summary>
        /// Imported Data from Excel.
        /// </summary>
        private List<DataSet> ImportedFiles;

        /// <summary>
        /// Converted Invoices.
        /// </summary>
        private List<MMOEntry> ConvertedEntries;

        #endregion

        #region Constructor

        /// <summary>
        /// Class Constructor
        /// </summary>
        /// <param name="importer"></param>
        public Controler(
                IXlsImporter importer
            )
        {
            Importer = importer;

            FilesToConvert = new List<string>();
            ImportedFiles = new List<DataSet>();
            ConvertedEntries = new List<MMOEntry>();
        }

        #endregion

        #region Logic

        /// <summary>
        /// Открывает диалог выбора файла.
        /// </summary>
        /// <returns>True в случае выбора файлов, False в случае отмены.</returns>
        public bool BrowseXLSFiles()
        {
            System.Windows.Forms.OpenFileDialog OpenDlg = new System.Windows.Forms.OpenFileDialog()
            {
                Multiselect = true,
                Filter = "xls files (*.xls)|*.xls|xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*",
                FilterIndex = 3,
                RestoreDirectory = true
            };

            if (OpenDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FilesToConvert.AddRange(OpenDlg.FileNames);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Imports data from Excel Files into corresponding Entities.
        /// </summary>
        /// <param name="filePathes">Array of pathes to files.</param>
        /// <returns>Operaion result.</returns>
        public bool ImportFiles(string[] filePathes = null)
        {
            // Если вызывают не через форму.
            if (filePathes != null)
            {
                FilesToConvert.AddRange(filePathes);
            }

            try
            {
                return Importer.ImportFromFile(FilesToConvert, ref ImportedFiles);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Peform the conversion.
        /// </summary>
        /// <returns></returns>
        public bool ProccesFiles()
        {
            bool result = false;

            try
            {
                foreach (var importedFile in ImportedFiles)
                {
                    ConvertedEntries.Add(DataMapper.ConvertToMMO(importedFile));
                }

                log.Info("Successfully converted " + ConvertedEntries.Count + " files.");
                result = true;
            }
            catch(Exception exc)
            {
                log.Error(exc.Message, exc);
                System.Windows.Forms.MessageBox.Show(
                    "Произошла ошибка!\n\n" + exc.Message +
                    "\n=================================\n" +
                    exc.StackTrace
                );
            }
            finally
            {
                FilesToConvert.Clear();
            }

            return result;
        }

        /// <summary>
        /// Export converted MMO entities into files.
        /// </summary>
        /// <param name="path">Folder to store file(s).</param>
        /// <returns>Operation result.</returns>
        public bool ExportFiles()
        {
            string finalPath = Properties.Settings.Default.export_dir;

            // If not in auto mode
            if (!Properties.Settings.Default.auto_mod)
            {
                #region Save Location Selection

                // Asking user where to save
                System.Windows.Forms.FolderBrowserDialog SaveDlg = new System.Windows.Forms.FolderBrowserDialog
                {
                    Description = "Выберите место сохранения",
                    ShowNewFolderButton = true
                };
                if (SaveDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    finalPath = SaveDlg.SelectedPath;
                    if (Properties.Settings.Default.log_debug)
                    { log.Debug("Save path chosen: \"" + finalPath + "\""); }

                }
                else
                {
                    // If they decline the request save in default
                    System.Windows.Forms.MessageBox.Show("Файлы будут сохранены в директорию экспорта.");
                    if (Properties.Settings.Default.log_debug)
                    { log.Debug("Saving to default directory: \"" + finalPath + "\""); }

                }

                #endregion
            }

            #region File(s) Writing

            foreach (var entry in ConvertedEntries)
            {
                if(entry == null)
                { continue; }

                if (entry.Name.Contains("/"))
                {
                    entry.Name = entry.Name.Replace("/", "дробь");
                }

                log.Info("Started exporting file \"" + System.IO.Path.Combine(finalPath, entry.Name + ".mmo") + "\"");

                using (System.IO.StreamWriter fileWriter =
                new System.IO.StreamWriter(
                        finalPath + "\\" + entry.Name + ".mmo",
                        false,
                        System.Text.Encoding.Default
                    )
                )
                {
                    // Sector 1
                    fileWriter.WriteLine(string.Join("\t", entry.Sector1_Header));

                    // Sector 2
                    fileWriter.WriteLine(string.Join("\t", entry.Sector2_Requisites));

                    // Sector 3
                    fileWriter.WriteLine(entry.Sector3_Commentary);

                    // Sector 4
                    // Для каждого товара четвёртого сектора
                    foreach (var item in entry.Sector4_ItemsList)
                    {
                        fileWriter.Write(string.Join("\t", item));

                        // Вставляем перенос строки после каждого товара кроме последнего
                        if (item != entry.Sector4_ItemsList[entry.Sector4_ItemsList.Count - 1])
                        {
                            fileWriter.WriteLine();
                        }
                    }
                }
            }

            #endregion

            log.Info("Successfully exported " + ConvertedEntries.Count + " entries.");
            ConvertedEntries.Clear();
            return true;
        }

        #endregion
    }
}
